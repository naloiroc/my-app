import React, { useState } from 'react';
import Questions from './Question'
import './index.css'

function Home() {
    const [showButton, setShowButton] = useState(true);
    const [showQuestion, setShowQuestion] = useState(false);
    const [questions, setQuestion] = useState(Questions);
    const [nowQuestion, setNowQuestion] = useState({});

    const handleGoDraw = () => {
        const unusedQuestions = questions.filter(m => m.isUsed === false);
        const q = unusedQuestions[Math.floor(Math.random() * unusedQuestions.length)];
        //const newQuestions = [...questions];
        //newQuestions[q.id - 1].isUsed = true;

        //setQuestion(newQuestions)
        setNowQuestion(q);
        setShowButton(false);
        setShowQuestion(true);
    } 

    const handleNext = () => {
        setShowButton(!false);
        setShowQuestion(!true);
    }

    return(
        <div className="background">
            <div className="panel">
                <h1 className="title">題目</h1>
                <div className="content">
                    {showQuestion && 
                        <p className="question">{nowQuestion.value}</p>
                    }
                    {showButton && 
                        <div className="go-draw" onClick={handleGoDraw}>開始</div>
                    }
                </div>
            </div>
            <div className="next" onClick={handleNext}></div>
        </div>

    );
}

export default Home;