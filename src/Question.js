const Questions = [
    { id: 1,  value: "愛", isUsed: false },
    { id: 2,  value: "海", isUsed: false },
    { id: 3,  value: "風", isUsed: false },
    { id: 4,  value: "星", isUsed: false },
    { id: 5,  value: "什麼", isUsed: false },
    { id: 6,  value: "一起", isUsed: false },
    { id: 7,  value: "來", isUsed: false },
    { id: 8,  value: "說", isUsed: false },
    { id: 9,  value: "誰", isUsed: false },
    { id: 10, value: "水", isUsed: false },
    { id: 11, value: "魚", isUsed: false },
    { id: 12, value: "手", isUsed: false },
    { id: 13, value: "聽", isUsed: false },
    { id: 14, value: "淚", isUsed: false },
    { id: 15, value: "分手", isUsed: false },
    { id: 16, value: "雨", isUsed: false },
    { id: 17, value: "彩虹", isUsed: false },
    { id: 18, value: "好", isUsed: false },
    { id: 19, value: "幸福", isUsed: false },
    { id: 20, value: "天", isUsed: false },
    { id: 21, value: "火", isUsed: false },
    { id: 22, value: "花", isUsed: false },
    { id: 23, value: "難過", isUsed: false },
    { id: 24, value: "記得", isUsed: false },
  ];

export default Questions;